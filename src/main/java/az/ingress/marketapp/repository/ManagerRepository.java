package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.repository.genericsearch.CustomSpecification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Collection;
import java.util.Optional;

public interface ManagerRepository extends JpaRepository<Manager, Long>, JpaSpecificationExecutor {
    Optional<Manager> findByName(String name);
}
