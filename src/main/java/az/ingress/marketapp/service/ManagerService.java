package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateManagerDto;
import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.repository.genericsearch.SearchCriteria;

import java.util.Collection;
import java.util.List;

public interface ManagerService {
    void create(Long marketId, Long branchId, CreateManagerDto managerDto);

    Collection<Manager> searchByName(List<SearchCriteria> searchCriteria);
}


