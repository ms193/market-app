package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateManagerDto;
import az.ingress.marketapp.mapper.ManagerMapper;
import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.repository.BranchRepository;
import az.ingress.marketapp.repository.ManagerRepository;
import az.ingress.marketapp.repository.genericsearch.CustomSpecification;
import az.ingress.marketapp.repository.genericsearch.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ManagerServiceImpl implements ManagerService {
    private final BranchRepository branchRepository;
    private final ManagerMapper managerMapper;
    private final ManagerRepository managerRepository;

    @Override
    public void create(Long marketId, Long branchId, CreateManagerDto managerDto) {
        var branch = branchRepository.findById(branchId).orElseThrow(RuntimeException::new);
        if (!branch.getMarket().getId().equals(marketId)) {
            throw new RuntimeException();
        }
        Manager manager = managerMapper.dtoToManager(managerDto);
        branch.setManager(manager);
        branchRepository.save(branch);
    }
    @Override
    public Collection<Manager> searchByName(List<SearchCriteria> searchCriteria) {
        CustomSpecification<Manager> specification = new CustomSpecification<>(searchCriteria);
        return managerRepository.findAll(specification);
    }
}



