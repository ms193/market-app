package az.ingress.marketapp.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class CreateBranchDto {


    String name;

    String address;

    Integer countOfEmployee;
    List<CreatePhoneDto> phones;

}


