
package az.ingress.marketapp.controller;
import az.ingress.marketapp.dto.CreateManagerDto;
import az.ingress.marketapp.dto.CreateMarketDto;
import az.ingress.marketapp.dto.ManagerDto;
import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.repository.genericsearch.SearchCriteria;
import az.ingress.marketapp.service.ManagerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market/{marketId}/branch/{branchId}/manager")
@Valid
public class ManagerController {
    private final ManagerService managerService;
    @PostMapping
    public void create(@PathVariable Long marketId,
                       @PathVariable Long branchId,
                       @RequestBody CreateManagerDto managerDto) {
        managerService.create(marketId, branchId, managerDto);
    }
    @PostMapping("/search")
    public Collection<Manager> search(@RequestBody List<SearchCriteria> searchCriteria) {
        return managerService.searchByName(searchCriteria);
    }

}
